from math import sqrt

critics = {
    'Lisa Rose': {
        'Lady in the Water': 2.5,
        'Snakes on а Plane': 3.5,
        'Just My Luck': 3.0,
        'Superman Returns': 3.5,
        'You, Me and Dupree': 2.5,
        'The Night Listener': 3.0
        },
    'Gene Seymour': {
        'Lady in the Water': 3.0,
        'Snakes on а Plane': 3.5,
        'Just My Luck': 1.5,
        'Superman Returns': 5.0,
        'The Night Listener': 3.0,
        'You, Me and Dupree': 3.5
        },
    'Claudia Puig': {
        'Snakes on а Plane': 3.5,
        'Just My Luck': 3.0,
        'The Night Listener': 4.5,
        'Superman Returns': 4.0,
        'You, Me and Dupree': 2.5
        },
    'Mick LaSalle': {
        'Lady in the Water': 3.0,
        'Snakes on а Plane': 4.0,
        'Just My Luck': 2.0,
        'Superman Returns': 3.0,
        'The Night Listener': 3.0,
        'You, Me and Dupree': 2.0
        },
    'Jack Matthews': {
        'Lady in the Water': 3.0,
        'Snakes on а Plane': 4.0,
        'The Night Listener': 3.0,
        'Superman Returns': 5.0,
        'You, Me and Dupree': 3.5
        },
    'Toby': {
        'Snakes on а Plane': 4.5,
        'You, Me and Dupree': 1.0,
        'Superman Returns': 4.0
        }
    }


# Повертає оцінку подібності person1 і person2 на основі евклідової відстані
def sim_distance(prefs, person1, person2):
    # Отримати список предметів, оцінених обома
    si = {}
    for item in prefs[person1]:
        if item in prefs[person2]:
            si[item] = 1
    # Якщо немає жодної загальної оцінки, повернути 0
    if len(si) == 0:
        return 0
    # Скласти квадрати різниць
    sum_of_squares = sum([
        pow(prefs[person1][item]-prefs[person2][item], 2)
        for item in prefs[person1] if item in prefs[person2]])
    return 1 / (1 + sum_of_squares)


# Повертає коефіцієнт кореляції Пірсона між p1 і p2
def sim_pearson(prefs, p1, p2):
    # Отримати список предметів, оцінених обома
    si = {}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item] = 1
    # Знайти число елементів
    n = len(si)

    # Якщо немає жодної загальної оцінки, повернути 0
    if n == 0:
        return 0

    # Обчислити суму всіх переваг
    sum1 = sum([prefs[p1][it] for it in si])
    sum2 = sum([prefs[p2][it] for it in si])

    # Обчислити суму квадратів
    sum1Sq = sum([pow(prefs[p1][it], 2) for it in si])
    sum2Sq = sum([pow(prefs[p2][it], 2) for it in si])

    # Обчислити суму добутків
    pSum = sum([prefs[p1][it]*prefs[p2][it] for it in si])

    # Обчислити коефіцієнт Пірсона
    num = n * pSum - (sum1 * sum2)
    den = sqrt((n*sum1Sq-pow(sum1, 2))*(n*sum2Sq-pow(sum2, 2)))
    if den == 0:
        return 0

    r = num/den

    return r


# Повертає список якнайкращих відповідностей для людини із словника prefs
# Кількість результатів в списку і функція подібності – необов'язкові параметри
def topMatches(prefs, person, n=5, similarity=sim_pearson):
    scores = [(similarity(prefs, person, other), other)
              for other in prefs if other != person]

    # Відсортувати список по убуванню оцінок
    scores.sort()
    scores.reverse()
    return scores[0:n]


# Отримати рекомендації для заданої людини (person),
# користуючись зваженим середнім оцінок,
# що отримані від решти всіх користувачів
def getRecommendations(prefs, person, similarity=sim_pearson):
    totals = {}
    simSums = {}
    for other in prefs:
        # порівнювати задану людину (person) з собою ж не потрібно
        if other == person:
            continue
        sim = similarity(prefs, person, other)

        # ігнорувати нульові і негативні оцінки
        if sim <= 0:
            continue
        for item in prefs[other]:

            # оцінювати тільки фільми, які задана людина (person) ще не дивилася
            if item not in prefs[person] or prefs[person][item] == 0:
                # Коефіцієнт подібності * Оцінка
                totals.setdefault(item, 0)
                totals[item] += prefs[other][item] * sim
                # Сума коефіцієнтів подібності
                simSums.setdefault(item, 0)
                simSums[item] += sim

    # Створити нормалізований список
    rankings = [(total / simSums[item], item)for item, total in totals.items()]

    # Повернути відсортований список
    rankings.sort()
    rankings.reverse()
    return rankings


def transformPrefs(prefs):
    result = {}
    for person in prefs:
        for item in prefs[person]:
            result.setdefault(item, {})

            # Обміняти місцями людину і предмет
            result[item][person] = prefs[person][item]
    return result
